---
layout: default
---

# Ah, so I sent you my CV, did I?

This is the full work history that used to be included in that ancient resume, which has been following me around since the early-to-mid 2000s in some form or other.

Jobs are in in reverse order from 2014 backwards.

---

**City of Sydney**  
Town Hall House  
Kent Street, Sydney

Senior Web Systems Administrator  
Oct 2011 –Apr 2014

I was initially brought in to manage City of Sydney’s internal SharePoint environments, but while in-progress, the role expanded considerably, with responsibility for deployment and maintenance of internal and external IIS and SharePoint server farms. I became part of a two-man DevOps team with responsibility for all of the City’s IIS and SharePoint environments, including the critical Online Business Gateway application.

**Dimension Data**  
Harrington Street  
The Rocks  
Sydney

Senior Applications Developer, SharePoint  
Dec 2010 – Sept 2011

My second stint at DiData, managing three separate SharePoint 2003, 2007 and 2010 farms, migrating content from older to newer, managing underlying SQL systems and developing new solutions. Mentoring of junior staff members on SharePoint 2010 and K2 Workflow


**Cargowise**  
Unit 3a, 72 O’Riordan Street  
Alexandria  
Sydney  
SharePoint Specialist  
Aug 2010 – Nov 2010  

Managing migration from SharePoint 2007 to 2010, developing collaboration and social media strategy, as well as ongoing SharePoint management and development. 

**Antares Solutions**  
SharePoint Consultant  
July-Aug 2010

Short term engagement consulting on SharePoint 2007 and 2010

**Gen-I Pty Ltd**  
SharePoint Consultant  
May 2010 – July 2010

Specialising in SharePoint delivery to Gen-I customers, with particular focus on migration and best practice. 3 month contract.

**Fujitsu Australia Limited**  
Oct 2009 – May 2010  
Senior Systems Engineer – SharePoint

Role involved mostly architecture, infrastructure, project forecasting and support work, though some hands-on code and customization work was required. Primarily worked with Qantas on large-scale SharePoint deployments, again who I’d worked with at Microsoft.

**Dimension Data**  
Aug 2009 – Oct 2009  
Technical Consultant, MS-AI

SharePoint technical consultant on a part-time, short-term basis. The role included onsite consultancy, presales and development, as well as infrastructure design and build. Primary client was WorkCover NSW, who I’d worked with at Microsoft.


**Microsoft Pty. Ltd.**  
April 2005 – June 2009

2005-2007  
Support Engineer, IIS and internet technologies  
Supporting premier customers on Web platform issues

2007-2009  
Support Engineer, SharePoint; Dedicated Support Engineer, SharePoint  
Supporting Premier customers exclusively on SharePoint 2003, 2007 and client SKUs

Won several awards during tenure, including the globally visible “Great People, Great Performance” award, for SharePoint related work with Telstra. The project involved cross-team collaboration within Microsoft, liaison with Telstra and a third-party integrator, and highly detailed performance analysis and troubleshooting with C-level visibility.

Role (and entire team) was made redundant in the global round of May 2009.

**Job Futures**  
Feb 2003 – April 2005  
Application Development manager

**Attik Pty Ltd**  
Sept 2000 – Feb 2003  
Senior Web Developer

**Leo Burnett**  
August - Sept 2000  
Contract Web Developer

**Orbis/E-Integration**  
Contract Web Developer  
June-August 2000

**Mallesons Stephen Jacques Solicitors**  
Web Database Developer  
May-June 2000

**Pres.co** (now called Wheel)  
Junior Web Developer rising to Middleweight Web Developer.  
May – November 1999
