---
layout: post
author: jasbro
title: Weekend Update 21 Apr 2024 - what's going on?
hint: Another week rolls by. SOme things change, some things stay the same
tags: diary, blog, meta,
---

Sunday April 21 2024. Liege Bastogne Liege day. Well, technically the day after because I got a little caught up in double-screening the race coverage alongside playing Steamworld Build on the XBox, so I'm catching up. 

A few updates today. The work situation took a sharp uptick over the last couple of weeks - clearly the quarter has ticked over and some budgets have been approved - and I suddenly have a lot of interest from a bunch of companies. I've also been doing a bit of continued learning as well as wasting a ton of my leisure time on games, so let's split this update up.

*1. Recreational and Professional computer things*

As mentioned, interviews and stuff are in-flight, and my former employer seems to have finalised a few things over transfer of assets to a new owner, so there's some cloud work in the pipeline. 

Meantime, I finished off the A Cloud Guru Terraform Associate course, which I'd left a couple of bits hanging from, investigated [AWS Finch](https://runfinch.com/) and did some more learning around some advanced Jekyll things, which I may get round to implementing on this blog in due course. I also had a long and very interesting chat with an ex-colleague on the state of DevOps in 2024 and whether indeed it has lost its way - a blog post of that is in draft on gitlab even as I write this, and will follow in time.

*2. Learning things that aren't computers*

My studies on Cymraeg continue. I finished the updated [Duolingo course](https://www.duolingo.com) content a couple of weeks back, so I'm back on the endgame "Daily Refresh" level for the second time. This update dropped after they announced they were [ceasing updates on the course](https://www.bbc.com/news/uk-wales-67197462), so NFI what happened there but it's definitely added some extra meat.

That being the case, I started on [Clozemaster's Fast Fluency](https://www.clozemaster.com/l/cym-eng) track, which is also a relatively new update, and I'm currently working my way through unit 6 and repeating the earlier units towards mastery.

This has displaced the [Glossika app](https://ai.glossika.com/), which assessed me as B2 (CEFR Upper Intermediate) a few months back. That app is _very_ unforgiving of spelling mistakes, so can be a bit frustrating. 

As ever, I'm halfway through several books, some of which I'll probably have to restart when I pick them up again, but I did learn one little gem from the QI Book of The Dead, which is that Marx and Engels, when they lived in London, used to play a game not unlike ISIHAC's "One Song To The Tune of Another", which is pleasing to say the least.

I also joined my local library. Apropos of nothing.

*3. Playing stupid games winning stupid prizes*

I've been enjoying Fallout 76's "America's Playground" update, got my jetpack, met the mothman, all that good stuff. In the interests of not getting psychologically scarred by MMO world, though, I've also played through ["Botany Manor"](https://www.botanymanor.com/), a rather relaxing gardening investigation sim that I recommend if you have time to kill walking around a victorian garden planting weird things and solving puzzles. Lastly, I jumped into Steamworld Build, which is a fun little world builder/miner/RTS type thing though not overly remarkable. Both are on XBox Game Pass.

*4. On not drinking.*

I gave up drinking this month. Not with any major fanfare or anything, but I decided I'm really not coping well with the day after a night on the beers like I used to. As a younger man I used to be able to have a big night out, get in at three, ride a bike 40km at six and be in work for standup. This is not a thing any more. 

After watching the Tour of Flanders coverage three weeks back I suffered an epic two day hangover from a relatively small selection of Belgian beers and decided that I'd watch the next weekend's Paris Roubaix sober instead. And I haven't had a drink since, haven't wanted one, and I think this sort of officially means I've given up drinking? It was always going to be on the cards eventually, I suppose. I do have a couple of sixpacks on non-alcoholic beer in the fridge, and I enjoyed a cheese plate with a non-alcoholic prosecco last night, and the world did not end. So... um.. yeah?



