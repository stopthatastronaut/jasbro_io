---
layout: post
author: jasbro
title: Crosswords 2! Guardian Friday Cryptic 29,367
hint: Second in the series, talking Gorman and taking on The Guardian's Cryptic 29,367 by Imogen
tags: diary, puzzles, crosswords, leisure
---

Dave Gorman - aka Bluth, aka Fed - dropped an experimental video onto YouTube recently, which I spotted while browsing YouTube and then spotted _again_ via Dave's Mastodon account. It's worth a watch. The idea is, essentially, that Dave creates a custom cryptic for a friend - in this case the actor Tony Gardner - and uses it almost as a casual interview or chat. Tony dives in and does the solution, Dave acts as mentor and coach and teases out some conversation as he goes. It's worth a watch on [Dave's Gormhub Youtube channel](https://www.youtube.com/watch?v=AeSH4Jy94m4). I think this has potential, although setting an entire themed crossword for each guest would probably make it somewhat labour-intensive, even for someone as productive as Gorman.

Now, as I write, it's about 7:30am on Saturday, and I had a fairly sleepless night last night. I'm not entirely sure why but it _did_ give me a chance to knock over the latest Guardian Friday cryptic - 29,367 for Friday 26 April - while lying awake. What follows is some explained clues and my full solved grid - don't click though if you're not up for a spoiler or two.

Now, some background before the spoilers. Guardian Cryptics generally trend more difficult through the week, feeling mild on Monday up to fairly fiendish on a Friday. This is fairly common across cryptic world - it also happens with the Sydney Morning Herald/The Age crossword (especially if DA has set the Friday puzzle), which was my go-to puzzle for a long time before the paywall closed on me.

This being the case, I expected this grid to be a toughie, and wasn't certain I'd get a solve immediately. However, the pins fell nicely for me on this occasion and I got it done in under 45 minutes as timed by Crossword Genius.

Again a warning - spoilers follow. Proceed accordingly.

<div x-data="{ open: false }">
    <p><button x-on:click="open = ! open">Click for spoilers!</button></p>
 
    <div x-show="open">
        <p>
        <span style="font-weight: bold"><a href="https://www.theguardian.com/crosswords/cryptic/29367">Guardian 29,367</a></span> does not appear to have a theme, but it does have some little nasties which needed a bit of digging in the thesaurus, wikipedia and the web in general. One in particular needed me to delve into the Guardian's archives a little, on which more anon.
        </p>

        <p>My gateway in came nice and early. I browsed all the clues first before trying to shuck the oyster, and on my second pass-through came to 1d.</p>

        <p class="cwclue">1. Quark cheese, hardly any remaining from the first minute (5)</p>

        <p class="cwexplain">
        
        My immediate thought here was that we're talking not about Quark the cheese - too obvious - but about quarks the subatomic particles. I'm aware, as you may be, that quarks come in "flavours", so I suspected that this clue was referring to a particular flavour of quark.<br /><br />

        Only one of the flavours has five letters, as a quick check on wikipedia confirmed, so I was pretty sure, but I wanted to justify my answer. It turns out that "minute" here refers to a simple "m", "from the first" means take first letters. Cheese, Hardly, Any, Remaining... plus that Minute.
        
        </p>

        <p class="cwanswer">CHARM</p>

        <p>A good start, top left corner.</p>

        <p>From here, expecting some of these clues to be tough, I browsed for further low-hanging fruit. Anagrams are often easy prey early doors, as are phrases split across multiple words, and short clues can often go quite easily. SO it was here. I cracked a four-letter in 24d</p>
        
        
        
        <p class="cwclue">24. Cliff is no end frightening (4)</p>

        <p class="cwexplain">"no end" suggests you're dropping the final letter, or possibly a couple of letters, off the end of a word. "is" feels like a pivot word separating the straight clue from the wordplay, sugesting I was dropping one or more letters from "frightening", to give me a word for a cliff.</p>

        <p class="cwanswer">SCAR</p>

        <p>This led me naturally on to 27a, which we now know ends with an R. </p>
        
        <p class="cwclue">27. Not quite sober, show dissent.</p>

        <p class="cwexplain">Again we have a clue that looks like we're dropping letters off the end of a word - "not quite". I fiddled around with synonyms off the top of my head here before hopping into the thesaurus and looking up "sober".
        <br /><br />
        One of the words that appeared was "demure". Dropping the "e" off that would give us "demur", which could mean "show dissent"
        </p>

        <p class="cwanswer">DEMUR</p>

        <p>Now, I'm not going to go through this clue by clue. For one thing I can't actually remember the order everything went in, but there was definitely a series of clues that started to tumble from here.</p>

        <p class="cwclue">4. Exposed without a spinner (7)</p>

        <p class="cwexplain">A spinner may be a top, but you don't have one so you're...</p>

        <p class="cwanswer">TOPLESS</p>
        
        <p class="cwclue">3. Ritual performed with zest in battle (10)</p>

        <p class="cwexplain">Looks like this is an anagram for the name of a battle, and chances are we're using that six-letter word at the start and then the other four letter word to get us to the ten letters needed. That "z" was helpful, because frankly there aren't that many battles with a Z in the name. Also, weirdly, it helped that the neighbourhood I grew up in had a bunch of streets named after famous battles. Sebastopol Street, Inkerman Street, Balaclava Street and... Osterley Street - Osterley is an old anglicized version of...</p>

        <p class="cwanswer">AUSTERLITZ</p>

        <p>This one was a plum clue, giving me a swathe of cross letters through the top left quarter. 9a now had an A and an S bracketing the middle three letters - so that was ALTOS. 1a, "Letter or card" is easy for a computer person who likes comedy - CHARACTER. 17a was also serendipitous</p>
        
        <p class="cwclue">17. They strum quietly before tragic heroine executed (7)</p>

        <p class="cwexplain">The word "strum" obviously takes us towards stringed instruments. And when you see "quietly" is often indicates a "p" (for the musical term _piano_ or _pianissimo_). That P comes before tragic heroine that's been executed. That would be a thing that strums - which is also a "p" word coming before a tragic heroine who has been executed - or perhaps _beheaded_. P + (ELECTRA - E) =</p>

        <p class="cwanswer">PLECTRA</p>

        <p>I did actually go and double check the <a href="https://en.wikipedia.org/wiki/Electra">story of Electra in Greek myth</a>. I was very confident of this one but I do like to justify my confidence, especially on a tough puzzle.</p> 
        <p>By now, that top left corner was really coming together. 2d gave me a little giggle</p>  
        
        <p class="cwclue">2. You had late dinner we hear, to get thin (9)</p>

        <p class="cwexplain">At ten, you ate</p>

        <p class="cwanswer">ATTENUATE</p>

        <p>11a took a little work with Wikipedia. </p>
        
        <p class="cwclue">11. Claudio always on time through two days and one more (10)</p>

        <p class="cwexplain">"always" felt like maybe "ever" to me. I also figured maybe "time" was a "t", so maybe "evert" or "tever" was in there. I had the "m" at the start and some crossing letters, so maybe the "days" referred to maybe "mon" for "Monday" at the start? So that might be "Montever", which brings us close to maybe an Italian name like Monteverde or Moneteverdi. <br /><br />
        Off to Wikipedia search - Is there a Claudio Moneterverdi? - Yes, there is</p>

        <p class="cwanswer">MONTEVERDI</p>

        <p>Things really started to roll now. SIOBHAN is an anagram. REREDOS is a screen in a church, but it's also re-redo with an "s" for small. JUST IN TIME refers to Archibishop Justin Welby going to prison - being given time. HOT DATE is a potentially exciting meeting. COMMUNIQUE is composed of COMMON with "on" dropped, coupled with UNIQUE. Two final fun ones:</p>

        <p class="cwclue">14. A white child of the upper classes is left behind</p>

        <p class="cwexplain">Tricky one. I needed to slice this up in my head so that the straight clue was "A white". CHild could then be "ch", Upper classes could be AB (class and class B) and "is left behind" might get us to LIS, giving us a white wine...</p>

        <p class="cwanswer">CHABLIS</p>


        <p class="cwclue">25. It stumps our cousin: he did you?</p>

        <p class="cwexplain">For this I had to hop into the Guardian's archives, specifically the obituaries. I had a suspicion that this might be referring to a setter of many classic Guardian puzzles, <a href="https://www.theguardian.com/crosswords/2013/nov/26/araucaria-john-graham">John Graham</a>.</p>

        <p class="cwanswer">ARAUCARIA</p>

        <p>I confess I had some trouble justifying this one, but it certainly fits. Likewise the last clue I got was a genuine trial. It crosses 25a, so given I had trouble justifying that solution, this one gave me some anxiety.</p>



        <p class="cwclue">21. Ancient Wood and a little metal in old instrument</p>

        <p class="cwexplain">I needed to go a little scattershot here. I hopped into a list of musical instruments on Wikipedia, came up more confused than before. I then delved in the thesaurus for "wood". That was a little vague, so I figured words like "timber" or "copse" or "thicket" might get me somewhere closer. I stumbled onto the word "shaw" for a wood, and the word "metal" suggested there was an M in there. But what the hell is a "shawm"? Another trip to the thesaurus revealed it's a kind of medieval oboe. </p>

        <p class="cwanswer">SHAWM</p>

        <p>And there we were done. Honourable mentions on this puzzle go to PARTI PRIS, another clue that needed some thesaurus work, RUBIK, CAP IN HAND and A BUM RAP, all lovely little clues.</p>

        <p><a href="https://www.theguardian.com/crosswords/cryptic/29367"><img src="/img/crosswords/guardian_cryptic_29367.jpeg" alt="solved crossword" /></a></p>

    </div>
</div>