---
layout: post
author: jasbro
title: Macbeth
hint: A thing arrived today, so I went off down a rabbit hole
tags: arts, cymraeg, shakespeare, macbeth, welsh
---

While I was playing around with Ubuntu Studio today, on a cruisy low-work day, the postman arrived. 

Always fun to have the postman arrive. I often don't remember what I've ordered - ADHD, natch - so it's often a nice little surprise surprise to get a knock at the door and have our friendly regular postie hand over something fun.

Today's item was indeed fun.

![A copy of Macbeth, translation into Welsh by T Gwynn Jones ](/img/blog/2A5D78A6-347E-451A-8A93-C24DAFB0DF21.jpg)

I found this particular little book on WOB, for about the AU$20 mark, and since I have a Welsh Background, I'm a continuing Cymraeg learner and I'm currently working on a [translation side project](https://mwyolyfrau.wales), I knew I should have it.

I also _love_ Macbeth, having seen the play performed several times, most notably at the 1998 Edinburgh Fringe, an open-air performance around the Royal Mile and in Greyfriars Kirkyard. Seeing Lady Macbeth's [somnambulistic monologue](https://www.shakespeare-online.com/plays/macbeth_5_1.html) staged against a background of graves was... quite something.

This particular copy has a date and a signature in the frontispiece, pinning it at 1942 but not a lot of other information on its background. So obviously I went on a little expedition into DuckDuckGo to see what I could find.

It seems copies of this little book do turn up from time to time - there are some on Etsy - so it's not a super rarity, but there's a [typescript copy held with the National Library of Wales](https://archives.library.wales/index.php/t-gwynn-jones-macbeth) which was used for a performance of the play in Cymraeg at the national Eisteddfod in 1938, and [signed by the author](https://en.wikipedia.org/wiki/T._Gwynn_Jones). 

More interestingly, the text was used in 1964 for [a televised version of the play](https://filmhubwales.org/en/films/macbeth/) which appears to pop up occasionally at film festivals and the like. And even more excitingly, [this is available on YouTube in full](https://www.youtube.com/watch?v=0sqooD95ibc). In fact, why don't I just embed it here?

<iframe width="560" height="315" src="https://www.youtube.com/embed/0sqooD95ibc?si=oIlHgMFTq6bcPU3X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I haven't been able to find a transcribed or digital version of the text yet, though I don't doubt one exists somewhere. It would be good to find as I'm not overly keen on damaging this octogenarian copy too much, though I have definitely had a quick leaf through to find some key scenes.

It appears this was Jones's second translation of Macbeth, the first being done in 1916, so it would be fascinating to find the earlier one, though I'm not hopeful of a quick discovery there. I also managed to find some other works by [T Gwynn Jones, on Open Library](https://openlibrary.org/authors/OL500376A/T._Gwynn_Jones). He seems like a fascinating character, prolific and thoughtful. Perhaps it might be fitting for me to spend some time in the near future getting this little book, and anything else I can dig up, OCR'd and transcribed for digital posterity. If I do that, it'll be discoverable via my [OpenLibrary profile](https://openlibrary.org/people/stopthatastronaut).

jasbro

