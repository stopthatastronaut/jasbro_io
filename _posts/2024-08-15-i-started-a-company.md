---
layout: post
author: jasbro
title: I Started A Company
hint: OK, so this is not the first company I've started. In common with about every web developer with ADHD I have a list of side projects the length of the Nile. This one, however, has clients and is actually the thing that is supporting me right now.
tags: diary, work, adhd, neurodiversity, devops, the devops company
---

Yep, the title kinda explains it, but what makes this different to the companies I've set up before and not done anything with? Well, this one started with a client and an income right out of the gate. Here's the story.

When my employer, GoodHuman Pty Ltd, went under back in March, I initially coasted along on my savings, which I had the luxury of doing, and hoped the company would find a buyer. As my savings started to come under pressure, I embarked on a couple of ill-advised attempts to re-enter the job market, neither of which proved to be a success. And the administration process ground along, frustratingly slowly. 

Then, in a sudden flurry of activity, my former employer's assets and IP came out the other side of the receivership process and were purchased by a new consortium, partly composed of former customers. I was asked to take the reins of the cloud assets and transfer them across to the new owner, which gave a welcome injection of income for a short while.

Once that was done, a few former employees were re-hired as part of a lean, mean, tiny startup. We'd lost surprisingly few customers and actually had new customers wanting to come and trial the product. The thing was viable. Fairly cash-strapped, but viable.

For my part, I had work coming my way but since a very small startup can't justify a full-time senior devops and cloud guy, I didn't get "re-hired" per se. 

However, I _was_ already invoicing for hours as part of the IP transfer activity, and the company still needed cloud skills and there's always something for someone like me to do. So we meandered towards an ongoing fixed-hours agreement, where I'm essentially a business owner, billing a client for a fixed, part-time package of hours every couple of weeks, and doing OK.

So, after a bunch of fiddling around, deciding what things should look like, consultations, procrastinations and general mayhem, the business name I registered back pre-pandemic for a side-project that never happened... happened.

It is called [The DevOps Company](https://d.evops.co/), and it's mine, and it's making money _right now_. 

Folks, I accidentally a startup.

So I've been on a learning curve for the last while, in between hours for what I guess I must now call "my client", learning how tax works, finding a freelancer to make me a logo, setting up email, setting up an bank account, setting up a website and a [suite of offerings](https://d.evops.co/services/) and wondering what the hell all this means.

I'll be offering all the usual devopsy cloud type stuff - architecture, coding, cost control, resiliency, observability - all that stuff I used to do for a pay packet, and also some allied services that I really enjoy doing but weren't part of my previous role. Content services like translation, proofreading and editing. Education services like seminars, training and mentoring. Business services like CV vetting and advisory work. Anything that needs a scarred and tattooed veteran of the Browser Wars, really.

And a YouTube channel, because you have to have a YouTube channel. And I've been talking about getting one for ages.

So, I guess if you need any of the things I do, and you can't necessarily afford a full-time devops guy, give me a shout. You can get to me via hello (at) d.evops.co, or my Mastodon, or the LinkedIn page that I'll probably have to re-activate.

Onward into the Bright New Future I guess!

Jason

