---
layout: post
author: jasbro
title: First assume a perfectly spherical website
hint: Why the web is dead and it's economics to blame
tags: web, enshittification, thought experiment
---

I woke up despondent today. 

This, in and of itself, is not an unusual occurence. It happens. It probably happens to all of us.

Today, though, was a little unusual, because I felt like I'd come to the end of a train of thought that's been nagging me for weeks. Months. Possibly years.

The web has been rotting. I'm no longer sure we can save it. My current opinion of the decline mirrors, in many ways, my opinions on Climate Change, Road Traffic, Access to Public and Private spaces, Government assistance, the world of work, Tragedies of The Commons and more.

I'll try to explain.

First, imagine a perfectly spherical website.

OK, I kid, but let's reduce our variables so we can get a clearer picture. Picture a website, a commercial website whose primary purpose is its ongoing commercial existence and the derivation of a profit. It can often endure a loss in the short-term, but overall it needs to make money.

We can imagine its secondary purpose if we like. Let's imagine it's the website of a newspaper - so it has a "mission" of "providing high quality news content to a customer base".

But that does not matter. What matter is that its purpose is to secure its own existence and profit. A commercial entity, above all else, needs to make money.

There are ways to make money on the web.

The most basic way to monetise is to show advertising. We're all familiar with this. We get free content, they get a little bit of our attention and some money somewhere changes hands. All very familiar.

However there is a mathematical relationship between showing more ads and making more money.

So a series of equations come into play.

- How MUCH advertising can I cram onto my page, before I alienate my visitors sufficiently to drive them away?

- How many of my visitors are repeat visitors? 

- Do repeat visitors even matter in a world of SEO and virality?

- Do my visitors hate me?

- Does it matter?

A company that is more concerned with the bottom line than any kind of mission will, perfectly logically, sell their soul for a few more ad impressions. They will fill the page _just_ far enough to piss you off, but not so far that you won't read to the end of the article anyway. And they will know you've reached the end because the tracking built into the page will confirm it, on a big old graph back in company HQ. Completion rates for readers. Holding steady. Send more ads. 

Make no mistake, they can measure exactly how much you're pissed off by having to view fifteen ads to read a five-paragraph news story. If you make it to the end, well, maybe you'll make it to the end with sixteen ads. Or seventeen.

Or maybe you, a total psychopath, will scroll even further, because the article is written with no clear conclusion and you're now so habituated to the ads that you'll scroll even further in search of one. Past all the Outbrain scams and Taboola bullshit, right down to the end where you will find no conclusion, no analysis, just another ad.

There is a graph, somewhere in math-space with one line showing revenue increasing as more ads are forced into your viewport, and another line with how much those ads are likely to drive you away.

And the people running "news" are homing in on exactly where those lines intersect, because that's their earning sweet-spot.

But we'll keep looking because that's where the news comes from now. And they'll keep getting positive signals about how much value the users get from all the "offered content" polluting their pages.

Can we fix this?

No

A capitalist system will always move at the whims of capital. There is no turning back. The commercial web is, inevitably, a BladeRunner future of massive glowing billboards, screaming their products at us 24/7/365. 

You can opt out. But you need news. You need entertainment. And so you WILL scroll to the end, delivering a precious signal to the people publishing this dreck that everything is fine and they can just keep going. 

Sure, you can install an ad blocker. But the [largest ad company in the world](https://www.statista.com/statistics/290629/digital-ad-revenue-share-of-major-ad-selling-companies-worldwide/) also _probably_ owns the browser you run, and do you think they're going to allow that shit to get out of hand?

They also pay the Mozilla Foundation a shedload of money every year to stay the default browser. You think they don't have one eye on how much ad-blocking browser extensions cost them?

The web has grown so large that only Massive Capital can move its needle, and that needle _will_ dance the way Capital demands.

The web is dead, and Capital did it.

