---
layout: post
author: jasbro
title: Using Alpine.js to load mastodon statuses
hint: An experiment!
tags: javascript, alpinejs, mastodon
---

So, I wanted to write a little loader for my Mastodon Toots on this here blog. So I cracked open Alpine.js and started hacking. Here's some of the things I - as someone who stopped writing front-end code around the era of JQuery - discovered.

### First speedbump: updating brain for Alpine.js

The last time I wrote any front-end interactivity, jQuery was king. I wrote a ton of dHTML back before frameworks took over, and never entirely moved away from that, so the model of - for example - React kinda doesn't fit in my brain. Having said that, I needed to knuckle down and I needed something lightweight, and not excessively complex.

So I landed a while back on [Alpine.js](https://alpinejs.dev/) as my preferred tool to learn.

My initial forays were very much just in-page interactivity using local data, some basic show/hide element stuff, some styling, you know the kind of thing. Tis was already fairly different to how I used to write Javascript back in the before time. Now, I have to update my brain to the Alpine model of the world and that started as a bit of a learning curve, but once the basics were apparent, it started to click together and I now feel like I could, _possibly_, write some front-end code again.

### Second speedbump: CORS

I have configured CORS on a lot of web-based services, but I have not yet run into CORS policies just straight-out stopping me loading content. So of course, I ran headlong into that almost immediately. CORS policies on your Mastodon `/outbox` endpoint will absolutely stop you just blithely loading content from Mastodon to a third-party webpage. 

I'll be writing a Proxy web service for this which will run on _my_ domain and talk to Mastodon as a back-end service, but for the purposes of the demo, I'm just grabbing the endpoint's JSON using powershell and storing it [here](outbox.json) I'll update this page when I've found time to write the actual web service I need.

### Third speedbump: Understanding how Toots are presented

Some toots have content, others don't. Some are just retoots, others are "quote toots". Some are media, some are not. I'm choosing not to overthink this and I'm just presenting toots that have content in a super-simple way.

### Fourth speedbump: Visual Studio Code keeps breaking my jekyll layouts.

Because for some reason I turned on "format on saving", Visual Studio Code has broken a whole bunch of stuff on the quiet and I'm not happy about it.

So that's turned off now.

So... here we go, client-side toot loading, simply done.

<script type="text/javascript">
  function mastodonLoad() {
    return {
      isLoading: false,
      statuses: null,
      fetchStatuses() {
        this.isLoading = true;
        // fetch('https://aus.social/users/drunkenmadman/outbox?page=true', {
        fetch('/outbox.json', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'redacted'
          }})
          .then(res => res.json())
          .then(data => {
            this.isLoading = false;
            this.statuses = data;
          });
      }
    }
  }
</script>

<div x-data="mastodonLoad()" x-init="fetchStatuses()">
  <template x-if="statuses.orderedItems">
    <template x-for="toot in statuses.orderedItems">
      <template x-if="toot.object.content">
        <div class="toot" x-html="toot.object.content"></div>
      </template>
    </template>
  </template>
</div>

Here's the code

```
<script type="text/javascript">
  function mastodonLoad() {
    return {
      isLoading: false,
      statuses: null,
      fetchStatuses() {
        this.isLoading = true;
        // fetch('https://aus.social/users/drunkenmadman/outbox?page=true', {
        fetch('/outbox.json', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'redacted'
          }})
          .then(res => res.json())
          .then(data => {
            this.isLoading = false;
            this.statuses = data;
          });
      }
    }
  }
</script>

<div x-data="mastodonLoad()" x-init="fetchStatuses()">
  <template x-if="statuses.orderedItems">
    <template x-for="toot in statuses.orderedItems">
      <template x-if="toot.object.content">
        <div class="toot" x-html="toot.object.content"></div>
      </template>
    </template>
  </template>
</div>
```

And that's that.

<p>&nbsp;</p>