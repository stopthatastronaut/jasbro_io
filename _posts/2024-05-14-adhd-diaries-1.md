---
layout: post
author: jasbro
title: The ADHD Diaries - part 1 - ADHD at work
hint: Bit of a meta post this one, very much about me, thee and ADHD, some of the changes I've made since my diagnosis and what I've kept exactly the same
tags: diary, work, tech commentary, adhd, neurodiversity
---

So, I've started a new role recently. I posted earlier about my job search hassles and how the first quarter of the year was terrible. And the starting a new role has made me rather thoughtful about my ways of working and how they fit into a new workplace.

Background: I was diagnosed with ADHD during the COVID lockdowns here in Melbourne. The upheaval changed a lot of things for a lot of people, and for those of us with undiagnosed neurodiversity, it laid bare all the coping strategies we'd previously relied on to cope in a neurotypical workplace.

A lot of them were patterns and practices, and some were employment-social crutches. I'm thinking particularly today, though, about the most obvious tool of the software developer's trade - the computer we sit at and type into.

I had and have a lot of little customisations which are, knowingly or otherwise, accessibility tweaks for my ADHD. This first week at a new role with a new laptop, back on Windows for the first time in four years or so, has highlighted a bunch of changes that I hadn't entirely considered before.

In common with many workplaces, my new employer has a very standardised Windows install - an SOE - with a LOT of Group Policy objects deployed. For someone used to being god mode on my machines, this feels like another kind of lockdown. And in common with the big old COVID kind of lockdown, this one stole a few of my coping strategies. Let's talk about a couple.

First, the team in charge saw fit to deploy a customised company Desktop Wallpaper, and to disable the ability to change it.

Innocuous change, no?

Well, no. It's absolutely not.

Let me explain a little. 

Some neurodiverse folks, not all by any means, but some, have a kind of "compulsive reading" thing. We see words, we read words. I didn't know it was a symptom at the time, but I used to drive my parents insane as a kid by reading out loud every single sign I saw. I see words, I read words. It is not voluntary for me, though I did grow out of doing it _out loud_.

I see words, I read words. 

And the team in charge of laptops at my new workplace saw fit to deploy a desktop background with a magenta-to-blue colour gradient *adorned with the full text of the company's value statements and mission*.

You can probably see the problem here.

Look, I'm sure someone somewhere thinks this is valuable, but what it actually does to me is _actively steal my attention_. I can be looking at a window over *here*, I can quickly hit windows+D to minimise all my windows, clear my decks and move to the next bit of the task and suddenly BRAIN SEE WORDS BRAIN READ WORDS and I suddenly have no idea what I came here for.

It's not fun. I ended up with a background level of anxiety that was way above where it should have been.

Worse, I run two external monitors at home and at work, so any time a desktop is visible, there they are. BRAIN SEE WORDS BRAIN READ WORDS good luck remembering what you were doing. 

I can absolutely multitask between windows and between blocks of text, that's not the issue here. It's the fact it ambushes me and I have to modify my ingrained workflow to try and avoid it.

For pretty much my entire career, I've unconsciously tried to keep my computing environment free of this kind of distraction. I have whiteboards, I keep them clean or I face away from them when not actively whiteboarding. I have multiple windows, I close, hide or minimise the ones I'm not actively using. I generally keep notebooks and documentation off my physical desktop when not actively using them. I used to hide all my menu bars, and I ran blank desktop backgrounds for YEARS, before graduating to mostly-blank or neutral backgrounds.

When Windows XP dropped, with its admittedly kinda fetching look, I immediately defaulted the theme back from the "iconic" blue bar with the orange button to the plain ordinary square-cornered windows 2000 look. I told myself this was for "performance" but in reality, it was cutting down on visual noise.

I didn't entirely realise why I was doing it, I just definitely worked better when there was less... CLUTTER.

So yeah, changing your staff's desktop environment so it permanently hijacks attention is kinda bad, and I'm not sure what issue it's solving in the first place.

I had a similar problem, but again I didn't entirely recognise it, when MacOS dropped an update recently and deployed an animated background, aka Live Wallpaper. Suddenly there's a moving background and my attention is stolen - but the worst part is that on a Mac the background is never\* entirely hidden. If like me you keep the dock visible, there's always a little bit of it peeking out in the corner of the screen. So I was having my attention destroyed without knowing why - like I was seeing ghosts out of the corner of my eye - until I figured it out and disabled the absolute living fuck out of it.

Honestly neurotypicals, why do you think this stuff is VALUABLE? 

Anyway, I tried to get the company to allow me to modify my desktop, but in line with the ethos that was becoming clear, Helpdesk was not empowered to actually elp. I called out that this would be a reasonable accomodation as required under Australian Workplace Law and I'd be happy to go to a tribunal about it. Still I got pushback. So I attempted to soldier on as best I could. Then, in a crowded and noisy office, with a manager pushing me to get a task done ASAP, and with no way of avoiding having my attention stolen, I snapped. I felt my heart rate going through the roof and distictive signs of an onrushing panic attack. 

I packed my backpack and walked out.

And I won't be going back. I resigned a couple of days later. A company that sees its users as interchangeable units all with the same needs is not a company I want to work for.

And neither should you, neurodivergent or not. 





