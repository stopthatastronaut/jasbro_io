---
layout: post
author: jasbro
title: Musings - Metadata, Work and the Broader Landscape, and a linkdump.
hint: A meta post about my current work situation and some of what else I've been up to
tags: diary, work, tech commentary, what am I reading
---

Well, another month rolls by and life in startup world, as sometimes happens, has got very complicated and then very static. 

GoodHuman [was placed into Administration](https://www.afr.com/work-and-careers/workplace/ndis-provider-goodhuman-forced-into-liquidation-owing-nearly-3m-20240308-p5fawd) in early March. It's a hazard of the startup scene, I guess, but this one was a little more of a bloodbath than perhaps is usual. The company was running in negative numbers for a period, based on promises of investment. Jam Tomorrow which never, in the end, arrived.

So, as happens every so often, I'm back on the job trail. It's still possible GoodHuman may be saved, but it's unlikely to be the same creature. So I'm chasing around for a new role, and the inevitable questions arise. Continue in the upper end looking for leadership and head of department roles, or decompress for a while with a senior IC role, perhaps a contract?

Hey ho, such is startup life.

In terms of **marginalia** and what's interesting me of late: 

[The Small Web](https://benhoyt.com/writings/the-small-web-is-beautiful/) continues to nibble away at my mind. With massive page sizes and bloat continuing to dominate the web tech space, it's refreshing to see [manifestoes like this one](https://rosswintle.uk/2024/02/a-manifesto-for-small-static-web-apps/) emerge, exhorting developers to go back to lightweight, simple and fast loading web pages, eschewing excess bells and whistles in favour of simplicity. There is indeed an undercurrent on my corner of the [fediverse](https://www.fediverse.to/) that the web has entirely lost its way, betrayed its founding principles and become a dystopia dominated by a few big players. 

There is more than a grain of truth to this.

To this end I have been fiddling around with [AlpineJS](https://alpinejs.dev/) alongside Django and - of course - Jekyll sites like this one, and having something of a nostalgia kick for the web of the late 90s.

The Large Language Model space increasingly appears to me to be the next big bubble. [Concerns about its rampacious energy hunger](https://www.scientificamerican.com/article/the-ai-boom-could-use-a-shocking-amount-of-electricity/) for computing resources lead me to believe it may never be a sustainable computing paradigm. Even cheap renewables are not free and cooling all that silicon is worrying from an environmental point of view. Add to that the unavoidable concerns that these large scale machine learning models are engaging in copyright infringment on a post-industrial scale continue to land me on the side of skepticism, which is not even to mention the hallucinations. ChatGPT has proven itself to me as capable of lying in multiple languages (on which more later) through the "Welsh Sentences From ChatGPT" Clozemaster pack, which is riddled with incorrect and nonsensical phrases and translations. [No, ChatGPT. The Welsh translation for "garlic bread" is not "thywod oerfel"](https://aus.social/@drunkenmadman/112024297470471891). That means "cold sand", at least where I grew up. Which brings me to the next bit...

**What am I reading?**

My studies of Cymraeg continue to be the dominant learning track outside of tech. I am into the Daily Refresh track on Duolingo, with some legendary levels left to clean up, which means I am de-emphasising that in favour of the [Fluency Fast Track on Clozemaster](https://www.clozemaster.com/l/cym-eng), as a way to broaden my vocabulary and allow me to consolidate where I'm at in terms of grammar. 

Outside of language study I've had limited blocks of time for reading, so I've been revisiting [James Felton's "You Really Don't Want To Know" and "Sunburn"](https://www.waterstones.com/author/james-felton/3492371). There is a significant Pile of Shame in terms of books I've not got round to yet. I'm hoping if I blog about this occasionally it'll keep me honest.

**What am I listening to?**

Podcasts mainly. Ed Zitron has a rather good new pod on Coolzone Media called [Better Offline](https://www.iheart.com/podcast/139-better-offline-150284547/) which sits nicely alongside TrashFuture in the "calling out tech" space, and CZM have recently done a [multi-parter on the history of the Luddites](https://pca.st/5e6ylfci), another topic with books sitting on my Pile of Shame. In terms of Comedy, I re-listened to an old [Comedian's Comedian with Stewart Lee](https://pca.st/8qnwb7ki) which gave excellent food for thought around what constitutes acceptable comedy, and harkened back to some shows I've been to which, shall we say, 'took a turn'. Remind me to talk about that. 

Anyway, that'll probably do for today, see you again later I guess.