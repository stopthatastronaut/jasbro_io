---
layout: post
author: jasbro
title: I updated this blog a bit
hint: I forked the theme and made some changes. Pray for me.
tags: diary, blog, meta, jekyll,
---

So yesterday's blog post was quite well received on the fedi. By which I mean a few people read it and I got some feedback saying "Do more!". So today I spent a little while actually making the site Better For Blogging.

First: The github minimal theme, which the original site was based on. This site started as my business card, essentially a thing to include with job applications or conference proposals, so people could get an idea of my professional *vibe*. Having decided to put a blog on it, the theme no longer really worked. 

I didn't want to pick another theme. I like the cleanliness and simplicity of this one. It vaguely reminds me of the first ever weblog I put up, self-built in Active Server Pages 3.0 in the early 2000s, before blogs were even called blogs, and now veery much defunct. But I did need some changes, so forking the theme was order of business number one.

I forked it across into my own github and looked into what would be required. Turns out I realy didn't need to do an entire fork, I could just copy index.html and post.html into my local _layouts folder and make the changes there for now. And so I did. I shuffled around some script and stylesheet files - the crossword blog post had included [Alpine.js](https://alpinejs.dev/) and a custom stylesheet - and made some tweaks, adding a navigation include to the left, fixing the footer properly while I was there.

The footer was something I'd wanted to do for a while. I'd been overriding it in CSS, because the default footer was github-centric:

```
footer p small {
    display: none;
}

footer p:after {
    content: "Runnning on GitLab Pages"
}
```

But now with the theme "forked" I can do as I wish with the thing. And I shall continue to do so.

Probably worth mentioning its been a long time since I've done any front-end work, and this has been refreshing.

Anyway, anything else interesting happening today? Not really. I spent the morning sulking on the sofa since I had a stomach upset of some kind. I got some stuff done in Fallout 76 while sulking, and eventually by early evening I was feeling a lot better.

Anyway, more to follow tomorrow, hopefully I'll have time to update the CI chain so it will post out to Mastodon when I post something new. I will also look at adding RSS support, which I'm not *so far* clear on how to do in Jekyll.
