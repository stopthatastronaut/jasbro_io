---
layout: post
author: jasbro
title: Crosswords! The New Quick-cryptic, and Guardian Cryptic 28,880
hint: First in what should become a series about cryptic crosswords.
tags: diary, puzzles, crosswords, leisure
---

The Guardian just launched a new cryptic crossword format, [and it's a beauty](https://www.theguardian.com/crosswords/quick-cryptic/1). 

It's the [Quick-cryptic](https://www.theguardian.com/crosswords/crossword-blog/2024/apr/06/a-new-guardian-crossword-the-quick-cryptic), and it's a small-ish, cryptic crossword that operates on a small subset of tricks. And, because this is designed as a gateway drug, it tells you which tricks it'll be using. It drops on Saturdays and I'll be following it even though I've been doing cryptics for years - because frankly in any skill it's ALWAYS good to refresh and strengthen your fundamentals from time to time.

I tried out the new Quick-cryptic last night. As a relatively experienced solver myself, I cleared it in under ten minutes, and then immediately tried it out on two novices.

The first cleared it with minimal assistance, the second needed a little more help but still got there.

The first was my dear friend [Happy Singer](https://mastodon.social/@happysinger), and after finishing the quick cryptic, wanted more. So I pointed him to this week's Quiptic. He managed to knock that one over too, and moved on to the Monday cryptic.

That one took a little longer, but by morning, and with a couple of small hints, that was completed too. A relative novice, three cryptics done in under 24 hours. A result!

I'd say that's a win for the new Quick-Cryptic format, and I'm quite taken with it.

Myself, I finished those three and revisted some older, incomplete, crosswords in my [Crossword Genius](https://www.crosswordgenius.com/) app. I landed on [Guardian Cryptic 28,880](https://www.theguardian.com/crosswords/cryptic/28880), of which I'd finished a mere few percent. I went on to complete it while trying to drop off to sleep. Spoilers most definitely follow. I'm not going to explain every answer, but I will explain some of the most fun clues and describe how I reached the full solution. Don't expand unless you want the crossword spoilered!

<div x-data="{ open: false }">

    <p><button x-on:click="open = ! open">Click for spoilers!</button></p>
 
    <div x-show="open">
        <p>
        <span style="font-weight: bold">Guardian 28,880</span> most definitely has a theme, and if you grew up in the UK, you may - possibly - spot it in short order.
        </p>

        <p>I did not - in fact - spot it in short order, despite my upbringing and despite getting 20a quite early on.</p>

        <p class="cwclue">20. Clothes with broader design (8)</p>

        <p class="cwexplain">This one is a combination, an anagram and an augment. You're looking at an anagram of "broader", as indicated by "design", but with the added wrinkle of adding "w" from "with". This is a nod to the common abbrevation of "with", such as in "w/water" or "w/cheese".</p>

        <p class="cwanswer">WARDROBE</p>

        <p>Nice. Good start. But one answer is not getting us anywhere. Continuing onwards. I also, early on, got 5d</p>

        <p class="cwclue">5. Writer, 'a god', pens introduction to Ulysses (6)</p>

        <p class="cwexplain">
        What's going on here then? Well, the word 'pens' can mean 'fences in' or 'traps'. 'introduction to Ulysses' immediately suggested to me that there's a 'u' in there somewhere. So we've probably got a 'u' penned inside something that means "a god". How about... Thor? A.. u.. thor.
        </p>

        <p class="cwanswer">AUTHOR</p>

        <p>
        OK, so that's good. We've got WARDROBE and AUTHOR. At this point I should perhaps have suspected a theme, and perhaps I would have, had I gone much further. For some reason, though, I left the puzzle idle for a chunk of time after my first session, but not before I got 17a. Once you know the theme, you might see why I went off the scent, but I think this is a goody.
        </p>

        <p class="cwclue">17. (10010050 + 1) ÷ y? It might make your head spin (7)</p>

        <p class="cwexplain">
        Baffling, no? Can't be a reference to binary, what with that 5 in there. Well, it can be a good idea when you see numbers in a clue to remember your roman numerals. In this case what you're looking at is 100,100,50 + 1 - or C,C,L + ONE - CCLONE. When you've got that, it's divided by Y, giving you...
        </p>

        <p class="cwanswer">CYCLONE</p>

        <p>And that was pretty much how I left it. For something like eighteen months.</p>

        <p>Coming back to the puzzle last night, I tabbed through a few clues looking for a crack into which I could jemmy my brain and twist. The key was 26 across.</p>

        <p class="cwclue">26. 5 British chessmen found here (5)</p>

        <p class="cwexplain">
            "British chessmen" elicited a picture in my head. The <a href="https://en.wikipedia.org/wiki/Lewis_chessmen">Lewis Chessmen</a>, clearly that's the answer! But wait... what's that 5 doing there at the start? Let's look up at clue 5d, which we solved earlier... Author? Let's fill in this clue...
        </p>

        <p class="cwanswer">LEWIS</p>

        <p>...and have a little think. Author. Lewis... Wardrobe? THERE'S A THEME.</p>

        <p>This was indeed the crack that allowed me to force open the entire solution. Obviously, the theme is <span style="font-weight: bold">Narnia</span>. Knowing this, I could attack some other clues.</p>

        <p class="cwclue">16. 510 -- Number of the beast?</p>

        <p class="cwexplain">Back to roman numerals here. L for 50, I for 1, an O for that zero, N for number and you've got the </p>

        <p class="cwanswer">LION</p>

        <p class="cwclue">3. Season well, including new thyme -- explain recipe for starters (6)</p>
        <p class="cwexplain">First letters, as indicated by 'for starters'</p>
        <p class="cwanswer">WINTER</p>

        <p class="cwclue">22. Hillary and Donald finally married in nude? Revolting! (6)</p>
        <p class="cwexplain">Edmund Hillary, conqueror of Everest. Donald finally = D Married = M, put those *in*... nude revolting = EUND</p>
        <p class="cwanswer">EDMUND</p>

        <p class="cwclue">27. Fleece follower's family (9)</p>
        <p class="cwexplain">Sheep's kin</p>
        <p class="cwanswer">SHEEPSKIN</p>

        <p>And from there the entire edifice fell into place, and in the end I'd spent a little over 55 minutes, spaced out over a couple of months of being baffled, with the majority of clues falling into place in the last ten or fifteen minutes. A very fun crossword, a classic in fact. And a nice little lesson in how sometimes one single piece of inspiration will turn an entire puzzle on a dime.</p>




    </div>
</div>

Whether you've gone for the spoilers or not, thanks for reading. It's nice to be blogging again on a smaller, more friendly web. And if you liked this post, feel free to ping me a message on [Mastodon](https://aus.social/@drunkenmadman) and let me know, because if it's useful or fun, I'll happily do some more.

<div>&nbsp;</div>

