---
layout: default
title: Blog
---
<h1>Latest Posts</h1>

<ul>
  {% for post in site.posts %}
    <li style="padding-bottom: 20px;">
      <h2><a href="{{ post.url }}" title="{{ post.hint }}">{{ post.title }}</a></h2>
      {{ post.excerpt }}

      <a style="color: lightgrey;" href="{{ post.url }}">read more...</a>


    </li>
  {% endfor %}
</ul>