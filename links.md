---
layout: default
---

# So I heard you like hyperlinks?

I sure do. Here's a few

- I do [Dry July](https://www.dryjuly.com/users/jason-brown-4) on sporadic occasions. Donate to me here.
- [That article where my team was called Australian DevOps Poster Boys](https://www.itnews.com.au/news/domain-goes-all-in-with-aws-407883)
- [That article where IT News gushed about our CI/CD Story ](https://www.itnews.com.au/news/domain-does-devops-392402/page0)
- [The Robot Army we built](https://www.slideshare.net/domaingroup/domains-robot-army)
- [I wrote it about a thing I did](https://tech.domain.com.au/2015/09/a-clutch-of-octopodes-moving-to-octopus-deploy-3-0-ha-edition/).
- Some people still think Octopus Deploy is just for deploying packages. Oh my. [Why would they define "deploy" in such a narrow way?](https://tech.domain.com.au/2015/05/octopusdeploy-as-command-and-control/)
- [I was testing Powershell before it was cool to test powershell](https://tech.domain.com.au/2015/10/using-pester-to-unit-test-powershell-scripts-continuously/). 
  Also, it is still not cool to test powershell. Sadly.
- [Fabien and I did a little talk at NDC Sydney](https://techconf.me/talks/14723)
- [We said they could ask us anything and they asked absolutely no emergency questions](https://www.youtube.com/watch?v=aPOx4YW012g)
- [That time I went to Singapore and accidentally spoke at a conference](https://powershellmagazine.com/2015/10/13/psconfasia-an-interview-with-powershell-expert-jason-brown/)
- I was a Cloud Architect on the team that built [Octopus Cloud v1](https://octopus.com/blog/introducing-octopus-cloud)
- [I used to organise a Skepticism Conference with some of my friends](https://www.slideshare.net/Stopthatastronaut/skepticamp-in-theory-and-practice)
- [I have no idea who would do this](https://www.smh.com.au/healthcare/vote-on-alternative-medicine-falls-victim-to-dark-arts-of-the-internet-20120210-1skv6.html)
- [Still less idea who would do this](https://replyto.mattjcan.com/)
- [And absolutely no idea on this one](https://thingsthatlastedlongerthantony.com)
