---
layout: default
---
# Hi there...

This is my business card. Do with it what you will. I was going to publish a full Curriculum Vitae here but I've been in this business too long and it takes forever, so *here's the potted summary*:

Feel free to get in touch, details below.

- I do DevOps stuff, often with Windows but often not. More so not these days.
- I've been doing that for ages (25 years now!) and I've seen and done a lot of things, and I'm proud of several of them. 
- Now I lead DevOps and SRE teams and I'm hoping I'll be good at that but in line with everything else in my entire career, I often have no idea if I'm succeeding or not.
- I'm the Principal Engineer at [The DevOps Company](https://d.evops.co/), a company I started in the 2010s but only got up and running in 2024
- I was diagnosed with ADHD during the COVID pandemic, which explained a lot and gave me a fresh perspective on things.
- I help to run the [DevUps mentoring community](https://devups.org/) on Discord
- I have particular interests in:
    - Continuous Delivery and Rapid Iteration
    - Disposable Infrastructure, as code
    - Python, PowerShell, F#, Terraform, things like that
    - Automated testing in a field notoriously not good at automated testing
    - Disability and Neurodiversity in teams
    - Miming the boring bits, or at least making a robot do them
    - Small web, privacy, inclusive technology
    - AWS, Azure, GCP and basically anything that has an API and can be made to scale up (or down) quickly
    - Internet of Things. My home is quite automated and I've built a whole bunch of toys using Raspberry Pi and Arduino, including at work
    - 3D printing and carpentry. Actually making physical objects!
    - Languages and Language Learning. I speak upper-intermediate Welsh and run a [volunteer translation project](https://mwyolyfrau.wales/). I'm also learning Dutch, German and Swedish,  and dabbling in a few other languages via online learning.
    - Wishing I could ride my bike more but not actually riding my bike more

You can look at a bunch of [links](/links) here

## But can I get in touch??

- You can email me using *pretty much anything* @jasbro.io - it all gets to me.
- The current version of my CV [is here](/assets/pdf/Jasbro-CV.pdf)
- You can find me on [LinkedIn](https://www.linkedin.com/in/stopthatastronaut/).
- I am on <a rel="me" href="https://mastodon.social/@CloudyOpsPoet">Mastodon</a>. 
- My non-work-related Mastodon account is at <a rel="me" href="https://aus.social/@drunkenmadman">Aus.social</a>.
- My [GitHub is here](https://github.com/stopthatastronaut/).
- I [used to blog over here](https://blog.d.evops.co/?p=534) but not for a loooong time and I may even shut that down.
- I [have a YouTube Channel](https://www.youtube.com/channel/UCBFd8fa6i5RGhw9KUltpLhg/about) in its very beginnings.

## Short-term engagements

Short-term contracts are generally best routed through [The DevOps Company](https://d.evops.co/). 

I used to publish rates here, but they are now handled through the company.

